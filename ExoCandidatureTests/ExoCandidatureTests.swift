//
//  ExoCandidatureTests.swift
//  ExoCandidatureTests
//
//  Created by savemywater on 01/05/2018.
//  Copyright © 2018 Prizm. All rights reserved.
//

import XCTest
@testable import ExoCandidature

class ExoCandidatureTests: XCTestCase {
    
    var controller:ViewController!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        controller = storyboard.instantiateInitialViewController() as! ViewController
    }
    
    override func tearDown() {
        super.tearDown()
        
        controller = nil
    }
    
    // test suppression like
    func testDelete() {
        controller.likesInteractor = UserLikesInteractor(delegate: controller)
        
        controller.likesInteractor.deleteLike(controller.likesInteractor.likes[1]) { (success) in
            if success {
                XCTAssertEqual(true, success)
            } else {
                XCTAssertEqual(false, success)
            }
        }
    }
    
    // Test confirmation like
    func testConfirm() {
        controller.likesInteractor = UserLikesInteractor(delegate: controller)
        
        controller.likesInteractor.confirmPendingLike(controller.likesInteractor.likes[1]) { (success) in
            if success {
                XCTAssertEqual(true, success)
                XCTAssertEqual(false, self.controller.likesInteractor.likes[1].isPending)
            } else {
                XCTAssertEqual(false, success)
            }
        }
    }
    
    // Test rejet like
    func testReject() {
        controller.likesInteractor = UserLikesInteractor(delegate: controller)
        
        controller.likesInteractor.rejectPendingLike(controller.likesInteractor.likes[1]) { (success) in
            if success {
                XCTAssertEqual(true, success)
            } else {
                XCTAssertEqual(false, success)
            }
        }
    }
}
