//
//  ViewController.swift
//  ExoCandidature
//
//  Created by Callum Henshall on 20/04/2018.
//  Copyright © 2018 Prizm. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast_Swift

// An example usage of UserLikesInteractor is shown here, you can modify this file.

class ViewController: UIViewController {

    var likesInteractor: UserLikesInteractor!
    @IBOutlet var listView: CustomListView!
    var items: [CustomListViewItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        navigationController?.navigationBar.barTintColor = UIColor(hexString: "#FD7556")
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationController?.navigationBar.tintColor = UIColor.white

        likesInteractor = UserLikesInteractor(delegate: self)

        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            let like = self.likesInteractor.likes[2]
            self.likesInteractor.deleteLike(
                like,
                completed: { (success) in
                    print("Delete completion: \(success)")
                }
            )
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            if let index = self.likesInteractor.likes.index(where: { $0.isPending == true }) {
                let like = self.likesInteractor.likes[index]
                self.likesInteractor.confirmPendingLike(
                    like,
                    completed: { (success) in
                        print("Confirm completion: \(success)")
                    }
                )
            }
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 15) {
            if let index = self.likesInteractor.likes.index(where: { $0.isPending == true }) {
                let like = self.likesInteractor.likes[index]
                self.likesInteractor.rejectPendingLike(
                    like,
                    completed: { (success) in
                        print("Reject completion: \(success)")
                    }
                )
            }
        }
        
        for l in self.likesInteractor.likes {
            items.append(CustomListViewItem(name: l.name))
        }
        
        self.listView.items = self.items
        listView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension ViewController: UserLikesInteractorDelegate {
    func userLikesInteractor(_ likesInteractor: UserLikesInteractor, added likes: [UserLikesInteractor.Like]) {
        print("Added: \(likes)")
        
        var newItems: [CustomListViewItem] = []
        for l in likes {
            newItems.append(CustomListViewItem(name: l.name))
        }
        
        self.listView.items.append(contentsOf: newItems)
        self.listView.reloadData()
    }

    func userLikesInteractor(_ likesInteractor: UserLikesInteractor, removed like: UserLikesInteractor.Like, at index: Int) {
        print("Removed at \(index): \(like)")
    }

    func userLikesInteractor(_ likesInteractor: UserLikesInteractor, updated like: UserLikesInteractor.Like) {
        print("Updated: \(like)")
    }
}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}
