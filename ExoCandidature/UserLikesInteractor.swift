//
//  UserLikesInteractor.swift
//  ExoCandidature
//
//  Created by Callum Henshall on 20/04/2018.
//  Copyright © 2018 Prizm. All rights reserved.
//

import Foundation

//
// This file shouldn't need to be modified... unless you find a bug :/
//

typealias CompletionClosure = (Bool) -> () // The Bool indicates success

fileprivate typealias LikeInformation = (name: String, country: String?, logo: URL?)

protocol UserLikesInteractorDelegate: class {
    func userLikesInteractor(_ likesInteractor: UserLikesInteractor, added likes: [UserLikesInteractor.Like])
    func userLikesInteractor(_ likesInteractor: UserLikesInteractor, removed like: UserLikesInteractor.Like, at index: Int)
    func userLikesInteractor(_ likesInteractor: UserLikesInteractor, updated like: UserLikesInteractor.Like)
}

//
// Interacts with the User's Likes
// Handles deleting and confirming and rejecting pending Likes
//

class UserLikesInteractor {
    fileprivate var mockedLikeIndex: Int = 0
    fileprivate lazy var mockedLikesInformation: [LikeInformation] = UserLikesInteractor.mockedLikesInformation

    private(set) var likes: [Like]
    weak var delegate: UserLikesInteractorDelegate?

    init(delegate: UserLikesInteractorDelegate? = nil) {
        self.likes = []
        self.delegate = delegate

        self.addRandomLikes(maxLikesToAdd: 10)
        self.startRandomlyAddingLikes()
    }

    private func addLikes(_ likes: [Like]) {
        self.likes.append(contentsOf: likes)
        delegate?.userLikesInteractor(self, added: likes)
    }

    private func removeLike(_ like: Like) {
        guard let index = likes.index(where: { $0.id == like.id }) else {
            return
        }

        likes.remove(at: index)
        delegate?.userLikesInteractor(self, removed: like, at: index)
    }

    private func confirmLike(_ like: Like) {
        var mutatingLike = like

        mutatingLike.isPending = false
        delegate?.userLikesInteractor(self, updated: like)
    }
}

extension UserLikesInteractor {
    func deleteLike(_ like: Like, completed: @escaping CompletionClosure) {
        mockAsyncAction { [weak self] (success) in
            if success {
                self?.removeLike(like)
            }
            completed(success)
        }
    }

    func confirmPendingLike(_ like: Like, completed: @escaping CompletionClosure) {
        mockAsyncAction { [weak self] (success) in
            if success {
                self?.confirmLike(like)
            }
            completed(success)
        }
    }

    func rejectPendingLike(_ like: Like, completed: @escaping CompletionClosure) {
        mockAsyncAction { [weak self] (success) in
            if success {
                self?.removeLike(like)
            }
            completed(success)
        }
    }
}

extension UserLikesInteractor {
    struct Like: CustomStringConvertible {
        static var idCounter: Int = 0

        let id: Int

        let name: String
        let country: String?
        let logo: URL?

        var isPending: Bool

        fileprivate init(name: String, country: String?, logo: URL?, isPending: Bool) {
            self.id = Like.idCounter
            Like.idCounter += 1

            self.name = name
            self.country = country
            self.logo = logo
            self.isPending = isPending
        }

        var description: String {
            return "Like(name: \(name), country: \(String(describing: country)), logo: \(String(describing: logo)), isPending: \(isPending))"
        }
    }
}

extension UserLikesInteractor {
    private func mockAsyncAction(completed: @escaping CompletionClosure) {
        // Wait between 0.1 and 3 seconds with 0.1 intervals
        let waitTime = Double(arc4random_uniform(30) + 1) / 10

        DispatchQueue.main.asyncAfter(deadline: .now() + waitTime) {
            let success = arc4random_uniform(2) == 1
            completed(success)
        }
    }

    private func startRandomlyAddingLikes() {
        // Wait between 5 and 10 seconds
        let waitTime = Double(arc4random_uniform(6) + 5)

        DispatchQueue.main.asyncAfter(deadline: .now() + waitTime) { [weak self] in
            self?.addRandomLikes()
            self?.startRandomlyAddingLikes()
        }
    }

    private func addRandomLikes(maxLikesToAdd: Int = 2) {
        // Add at least half the maximum number of Likes
        let median = maxLikesToAdd / 2
        let likesToAdd = Int(arc4random_uniform(UInt32(maxLikesToAdd - median + 1))) + median

        let likes: [Like] = (0..<likesToAdd).map { (_) in self.mockedLike() }
        addLikes(likes)
    }

    private func mockedLike() -> Like {
        let likeInformation: LikeInformation = mockedLikesInformation[mockedLikeIndex]

        mockedLikeIndex += 1
        if mockedLikeIndex == mockedLikesInformation.count {
            mockedLikeIndex = 0
        }

        let isPending: Bool = arc4random_uniform(2) == 1

        return Like(
            name: likeInformation.name,
            country: likeInformation.country,
            logo: likeInformation.logo,
            isPending: isPending
        )
    }

    private class var mockedLikesInformation: [LikeInformation] {
        return [
            (name: "jazz-spirit", country: "Slovakia", logo: URL(string: "http://db.radioline.fr/pictures/radio_03b3bfe1ce80cd33a8d0fda812e92583/logo200.jpg")),
            (name: "SMOOTH JAZZ 247", country: "France", logo: URL(string: "http://db.radioline.fr/pictures/radio_44a44b70e44106bee8887d24b408857a/logo200.jpg")),
            (name: "BCN Jazz", country: "Spain", logo: URL(string: "http://db.radioline.fr/pictures/radio_bda23a252362f637a615842b6a98e761/logo200.jpg")),
            (name: "Jazz1Max", country: "France", logo: URL(string: "http://db.radioline.fr/pictures/radio_2d8c7e33980bcc5fab0420c113245cac/logo200.jpg")),
            (name: "Jazz FM 91", country: "Canada", logo: URL(string: "http://db.radioline.fr/pictures/radio_0a600338a72c752cdeab371dbfb5d869/logo200.jpg")),
            (name: "NRK Jazz", country: "Norway", logo: URL(string: "http://db.radioline.fr/pictures/radio_cbf60314af16f335ba4231524b3dd0b2/logo200.jpg")),
            (name: "Jazz Radio", country: "France", logo: URL(string: "http://db.radioline.fr/pictures/radio_e40f696f12bff726f5365be2d7bf2643/logo200.jpg")),
            (name: "1 Splash Jazz", country: "Spain", logo: URL(string: "http://db.radioline.fr/pictures/radio_6642ffc277f6b72efc8d4ac94201bc7c/logo200.jpg")),
            (name: "Concertzender Jazz", country: "Netherlands", logo: URL(string: "http://db.radioline.fr/pictures/radio_0828f4e67a86b0357b2482abdd3bbb64/logo200.jpg")),
            (name: "Wav Jazz", country: "United States", logo: URL(string: "http://db.radioline.fr/pictures/radio_d0be00f03be2ec9bc5e63a06a63ecb55/logo200.jpg")),
            (name: "WGBH Jazz", country: "United States", logo: URL(string: "http://db.radioline.fr/pictures/radio_30cb9fb842c8881930c31c339085cfb2/logo200.jpg")),
            (name: "WSQX Jazz", country: "United States", logo: URL(string: "http://db.radioline.fr/pictures/radio_e4a8367882c8621b73e440598a7a65d6/logo200.jpg")),
            (name: "Radio 1 Jazz", country: "Greece", logo: URL(string: "http://db.radioline.fr/pictures/radio_6a0d8a65b27f72d6e57cb68152bec576/logo200.jpg")),
            (name: "ABC Jazz", country: "Australia", logo: URL(string: "http://db.radioline.fr/pictures/radio_49c5f5337cb0ece53ab20c566fe24774/logo200.jpg")),
            (name: "101.ru - Jazz", country: "Russia", logo: URL(string: "http://db.radioline.fr/pictures/radio_e72bfceffdf94521b5b54e688d959a8d/logo200.jpg")),
            (name: "100% Jazz - 104,9 FM", country: "Réunion", logo: URL(string: "http://db.radioline.fr/pictures/radio_bb61384656a92bcc34dcfa18d364823a/logo200.jpg")),
            (name: "ABC Jazz", country: "United States", logo: URL(string: "http://db.radioline.fr/pictures/radio_a8d1df7dff829aa12fc110018db79d95/logo200.jpg")),
            (name: "Allegro Jazz", country: "France", logo: URL(string: "http://db.radioline.fr/pictures/radio_6a727f520ecc99aef0fd95f85c973cf6/logo200.jpg")),
            (name: "RSI JAZZ", country: "Italy", logo: URL(string: "http://db.radioline.fr/pictures/radio_c3695330d6926c46ee042db87ce18a49/logo200.jpg")),
            (name: "Jazz Medley", country: "Brazil", logo: URL(string: "http://db.radioline.fr/pictures/radio_9940cede37c7d64fe62f360a48b7dfd8/logo200.jpg")),
            (name: "#1 JAZZ RADIO", country: nil, logo: URL(string: "http://db.radioline.fr/pictures/radio_43394ad590be9a3313fd0472fae83208/logo200.jpg")),
            (name: "Radio Jazz", country: "Denmark", logo: URL(string: "http://db.radioline.fr/pictures/radio_f872e31af863720287eba12a290728af/logo200.jpg")),
            (name: "RadioSkyMusic Jazz", country: "Canada", logo: URL(string: "http://db.radioline.fr/pictures/radio_069d546a4245ecbca747d08c91f394bb/logo200.jpg")),
            (name: "KUVO Jazz", country: "United States", logo: URL(string: "http://db.radioline.fr/pictures/radio_6833e3d0d91c6bb1e9b14f81c0cc48fb/logo200.jpg")),
            (name: "Nostalgie Jazz", country: "Belgium", logo: URL(string: "http://db.radioline.fr/pictures/radio_eb905296bd761c4cd3f3275e65aff9df/logo200.jpg"))
        ]
    }
}

