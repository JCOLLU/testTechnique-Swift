//
//  HexagonCollectionView.swift
//  ExoCandidature
//
//  Created by savemywater on 26/05/2018.
//  Copyright © 2018 Prizm. All rights reserved.
//

import Foundation
import UIKit

public class CustomListView: UIView {
    
    // Variable
    var items: [CustomListViewItem] = []
    var numberOfItemsInRow = 3
    var padding: CGFloat = 8
    var paddingInset : CGFloat = 5
    var scrollView: UIScrollView!
    var contentView: UIView!
    var backgroundImage: UIImage! = #imageLiteral(resourceName: "hexagon")
    var backgroundImageFocused: UIImage! = #imageLiteral(resourceName: "hexagon_clicked")
    var fontSize: CGFloat = 14
    
    // Inspectable pour storyboard
    @IBInspectable var NumberOfItemsInRow: Int {
        get {
            return numberOfItemsInRow
        }
        set {
            numberOfItemsInRow = newValue
        }
    }
    
    @IBInspectable var Padding: CGFloat {
        get {
            return padding
        }
        set {
            padding = newValue
        }
    }
    
    @IBInspectable var PaddingInset: CGFloat {
        get {
            return paddingInset
        }
        set {
            paddingInset = newValue
        }
    }
    
    @IBInspectable var FontSize: CGFloat {
        get {
            return fontSize
        }
        set {
            fontSize = newValue
        }
    }
    
    @IBInspectable var BackgroundImage: UIImage {
        get {
            return backgroundImage
        }
        set {
            backgroundImage = newValue
        }
    }
    
    @IBInspectable var BackgroundImageFocused: UIImage {
        get {
            return backgroundImageFocused
        }
        set {
            backgroundImageFocused = newValue
        }
    }
    
    // Fonction
    override public func willMove(toSuperview newSuperview: UIView?) {
        self.layOutItems()
    }
    
    override public func layoutSubviews() {
        self.reloadData()
    }
    
    public func reloadData() {
        for subView in self.subviews {
            subView.removeFromSuperview()
        }
        if self.numberOfItemsInRow < 2 {
            self.numberOfItemsInRow = 3
        }
        self.layOutItems()
    }
    
    func layOutItems() {
        self.scrollView = UIScrollView(frame: self.bounds)
        scrollView.backgroundColor = .clear
        self.addSubview(self.scrollView)
        
        self.contentView = UIView(frame: CGRect(x: 0, y: 16, width: self.scrollView.bounds.width, height: 0))
        self.contentView.backgroundColor = .clear
        self.scrollView.addSubview(self.contentView)
        
        for x in 0...self.numberOfRows(numberOfItemsInRow: self.numberOfItemsInRow) {
            self.drawRow(rowViewTag: x)
        }
    }
    
    func sortedDataSource() -> [Int:[CustomListViewItem]] {
        var honeyCombMutableObjects = self.items
        var sortedDataSource: [Int:[CustomListViewItem]] = [:]
        
        for x in 0..<honeyCombMutableObjects.count {
            if self.isEven(x) {
                for _ in 0..<self.numberOfItemsInRow {
                    if sortedDataSource[x] != nil {
                        if honeyCombMutableObjects.count > 0 {
                            sortedDataSource[x]?.append(honeyCombMutableObjects.removeFirst())
                        }
                    }
                    else {
                        if honeyCombMutableObjects.count > 0 {
                            sortedDataSource[x] = [honeyCombMutableObjects.removeFirst()]
                        }
                    }
                    
                }
            } else {
                for _ in 0..<self.numberOfItemsInRow - 1 {
                    if sortedDataSource[x] != nil {
                        if honeyCombMutableObjects.count > 0 {
                            sortedDataSource[x]?.append(honeyCombMutableObjects.removeFirst())
                        }
                    }
                    else {
                        if honeyCombMutableObjects.count > 0 {
                            sortedDataSource[x] = [honeyCombMutableObjects.removeFirst()]
                        }
                    }
                }
            }
        }
        
        return sortedDataSource
    }
    
    func numberOfRows(numberOfItemsInRow: Int) -> Int {
        var numberOfRows: Int = 0
        
        if self.items.count > 0 {
            
            let average: Double = ((2 * Double(numberOfItemsInRow)) - 1) / 2
            let decimal: Double = 1 - ((Double(numberOfItemsInRow) - 1) / Double(numberOfItemsInRow))
            
            let number: Double = Double(self.items.count) / average
            
            numberOfRows = Int(floor(number))
            
            let remainder: Double = number - Double(numberOfRows)
            
            if remainder >= decimal {
                numberOfRows += 1
            }
        }
        
        return numberOfRows
    }
    
    func drawRow(rowViewTag: Int) {
        let remainingItemsCount = self.sortedDataSource()[rowViewTag]?.count
        if remainingItemsCount != nil {
            
            let itmesInRow = self.numberOfItemsInRow
            let width = self.frame.width
            
            let rowView = UIView()
            rowView.tag = rowViewTag
            rowView.frame.size = CGSize(width: width, height: width)
            rowView.backgroundColor = .clear
            
            for x in 0...remainingItemsCount! - 1 {
                
                let item = CustomButton()
                item.contentEdgeInsets = UIEdgeInsets(top: paddingInset, left: paddingInset, bottom: paddingInset, right: paddingInset)
                item.tintColor = .white
                item.backgroundImage = self.backgroundImage
                item.backgroundImageFocused = self.backgroundImageFocused
                item.titleLabel?.numberOfLines = 0
                item.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
                if UIDevice.current.userInterfaceIdiom == . pad {
                    item.titleLabel?.font = UIFont.systemFont(ofSize: 30)
                }
                item.titleLabel?.adjustsFontSizeToFitWidth = true
                item.titleLabel?.lineBreakMode = .byWordWrapping
                item.titleLabel?.textAlignment = .center
                item.titleLabel?.minimumScaleFactor = 0.5
                item.setTitle(self.sortedDataSource()[rowViewTag]?[x].name, for: .normal)
                item.tag = x
                let itemWidth = (width - ((CGFloat(itmesInRow) + 1) * self.padding)) / CGFloat(itmesInRow)
                item.frame.size = CGSize(width: itemWidth, height: itemWidth * (114 / 103))
                rowView.frame.size.height = itemWidth - 15 + padding
                
                var centerX: CGFloat!
                
                if self.isEven(rowViewTag) {
                    centerX = ((CGFloat(x + 1)) * self.padding) + ((item.frame.width / 2) * ((2 * CGFloat(x)) + 1))
                } else {
                    let widePadding = (width - (CGFloat(itmesInRow - 1) * itemWidth) - (CGFloat(itmesInRow - 1) * self.padding)) / 2
                    centerX = widePadding + ((CGFloat(x)) * self.padding) + ((item.frame.width / 2) * ((2 * CGFloat(x)) + 1)) + (self.padding / 2)
                }
                
                let centerY = rowView.center.y
                item.center = CGPoint(x: centerX, y: centerY)
                item.setBackgroundImage(backgroundImage, for: .normal)
                item.setBackgroundImage(backgroundImageFocused, for: .focused)
                rowView.addSubview(item)
            }
            
            rowView.frame.origin = CGPoint(x: 0, y: CGFloat(rowViewTag) * rowView.frame.height)
            
            self.scrollView.contentSize.height += rowView.frame.height
            self.contentView.frame.size.height += rowView.frame.height
            
            if rowViewTag == self.numberOfRows(numberOfItemsInRow: self.numberOfItemsInRow) - 1 {
                self.scrollView.contentSize.height += 6 * self.padding
            }
            
            self.contentView.addSubview(rowView)
        }
    }
    
    func isEven(_ number: Int) -> Bool {
        
        var result: Bool
        
        if number % 2 == 0 {
            result = true
        }
        else {
            result = false
        }
        return result
    }
    
}

class CustomButton: UIButton {
    
    var backgroundImage: UIImage! = #imageLiteral(resourceName: "hexagon")
    var backgroundImageFocused: UIImage! = #imageLiteral(resourceName: "hexagon_clicked")
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        superview?.superview?.bringSubview(toFront: superview!)
        superview?.bringSubview(toFront: self)
        self.setBackgroundImage(self.backgroundImageFocused, for: .normal)
        
        UIView.animateKeyframes(withDuration: 0.4, delay: 0, options: .allowUserInteraction, animations: {
            self.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: { (_) in
            UIView.animateKeyframes(withDuration: 0.4, delay: 0, options: .allowUserInteraction, animations: {
                self.transform = CGAffineTransform.identity
            }, completion: { (_) in
                self.setBackgroundImage(self.backgroundImage, for: .normal)
            })
        })
    }
    
}

class CustomListViewItem {
    
    public var name: String!
    
    init(name:String) {
        self.name = name
    }
    
}
